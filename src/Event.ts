export class Event {
    id: number
    title: string 
    allDays?: boolean
    start: Date
    end: Date
    constructor(id:number,title:string,start:Date,end:Date){
        this.id = id
        this.title= title
        this.start= start
        this.end = end

    }
}