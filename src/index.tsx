import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import  App  from './App';
import * as serviceWorker from './serviceWorker';
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import { Switch, Router, Route} from 'react-router';
import { Forum } from './Forum';
import { createBrowserHistory } from 'history'
import { Contact } from './Contact';
let history= createBrowserHistory()
ReactDOM.render( <Router history={history}>  
<Route path="/" component = {App} />
<Route path='/contact' component={Contact}/>
<Route path = "/forum" component = {Forum} />

</Router>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
