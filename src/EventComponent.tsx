import  React from 'react'
import moment from 'moment'
import 'moment/locale/fr'
import { Event } from './Event'

interface EventProps {
    event: Event
}
interface State {

}
export class EventComponent extends React.Component<EventProps> {
    constructor(props: EventProps){
        super(props)
       // this.state
    }

    render(){
        const { event } = this.props
        return(
            <React.Fragment>
                <div className="time">{event.start} - {event.end}</div>
                <div className="title">{event.title}</div>
            </React.Fragment>
        )
    }
}